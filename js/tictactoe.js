var z1 = 0;
var z2 = 0;
var current = 'x';
var state = "running";

function setScore(xoro){
    if(xoro=='x'){
        z1++;
    }else{
        z2++;
    }
    score.innerHTML = z1 + "-" + z2;
    reset();
}

function clicked(num){
    if(document.getElementById("ttt"+num).innerHTML != 'x' && document.getElementById("ttt"+num).innerHTML != 'o'&&state=="running"){
        document.getElementById("ttt"+num).innerHTML = current;
        player();
        checkWinner();
    }
}

function player(){
    if(current == 'x'){
        current = 'o';
    }else{
        current = 'x';
    }
}

function checkWinner(){
    if(ttt1.innerHTML == ttt2.innerHTML && ttt1.innerHTML == ttt3.innerHTML && ttt1.innerHTML != ''){
        setScore(ttt1.innerHTML);
        return;
    }else if(ttt4.innerHTML == ttt5.innerHTML && ttt4.innerHTML == ttt6.innerHTML && ttt4.innerHTML != ''){
        setScore(ttt4.innerHTML);
        return;
    }else if(ttt7.innerHTML == ttt8.innerHTML && ttt7.innerHTML == ttt9.innerHTML && ttt7.innerHTML != ''){
        setScore(ttt7.innerHTML);
        return;
    }else if(ttt1.innerHTML == ttt5.innerHTML && ttt1.innerHTML == ttt9.innerHTML && ttt1.innerHTML != ''){
        setScore(ttt1.innerHTML);
        return;
    }else if(ttt7.innerHTML == ttt5.innerHTML && ttt7.innerHTML == ttt3.innerHTML && ttt7.innerHTML != ''){
        setScore(ttt7.innerHTML);
        return;
    }else if(ttt1.innerHTML == ttt5.innerHTML && ttt1.innerHTML == ttt9.innerHTML && ttt1.innerHTML != ''){
        setScore(ttt1.innerHTML);
        return;
    }else if(ttt1.innerHTML == ttt4.innerHTML && ttt1.innerHTML == ttt7.innerHTML && ttt1.innerHTML != ''){
        setScore(ttt1.innerHTML);
        return;
    }else if(ttt2.innerHTML == ttt5.innerHTML && ttt2.innerHTML == ttt8.innerHTML && ttt2.innerHTML != ''){
        setScore(ttt2.innerHTML);
        return;
    }else if(ttt3.innerHTML == ttt6.innerHTML && ttt3.innerHTML == ttt9.innerHTML && ttt3.innerHTML != ''){
        setScore(ttt3.innerHTML);
        return;
    }

    for(i=1; i<10; i++){
        if(document.getElementById("ttt"+i).innerHTML != 'x' && document.getElementById("ttt"+i).innerHTML != 'o'){
            return;
        }
    }
    reset();
}

async function reset(){
    state = "paused";
    await sleep(1000);
    for(i=1; i<10; i++){
        document.getElementById("ttt"+i).innerHTML = "";
    }
    state = "running";
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
